import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import cartReducer from "./modules/Cart/reducer";
import shopReducer from "./modules/Products/reducer";

const reducers = combineReducers({
    products: shopReducer,
    productCart: cartReducer,
    removeCart: cartReducer
});
const store = createStore(reducers, applyMiddleware(thunk));

export default store;