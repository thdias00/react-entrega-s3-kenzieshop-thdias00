import { LIST_PRODUCTS } from "./actionTypes";

const shopReducer = (state = {}, action) => {
    switch (action.type) {
        case LIST_PRODUCTS:
            return action.products;
        default:
            return state;
    }
}

export default shopReducer;