import { listCart, removeToCart } from "./actions";

export const listCartThunk = (item) => {
    return (dispatch) => {
        const itens = JSON.parse(localStorage.getItem('productCart')) || [];
        const itemAdd = itens.find((product) => product.name === item.name);
        if (itemAdd === undefined) {
            itens.push(item);
            localStorage.setItem('productCart', JSON.stringify(itens));
            dispatch(listCart(item));
        }
    }
}

export const removeToCartThunk = (item) => (dispatch, getStore) => {
    console.log('ok')
    const { productCart } = getStore();
    const itens = productCart.filter((product) => product.name !== item.name);
    localStorage.setItem('productCart', JSON.stringify(itens));
    console.log(itens)
    dispatch(removeToCart(itens));
}