import { LIST_CART, REMOVE_TOCART } from "./actionTypes";

const cartReducer = (state = JSON.parse(localStorage.getItem('productCart')) || [], action) => {
    switch (action.type) {
        case LIST_CART:
            const { productCart } = action;
            return [...state, productCart];
        case REMOVE_TOCART:
            const { removeCart } = action;
            return removeCart
        default:
            return state;
    }
}
export default cartReducer;