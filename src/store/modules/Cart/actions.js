import { LIST_CART } from "./actionTypes";
import { REMOVE_TOCART } from "./actionTypes";

export const listCart = (productCart) => ({
    type: LIST_CART,
    productCart,
});

export const removeToCart = (removeCart) => ({
    type: REMOVE_TOCART,
    removeCart,
})