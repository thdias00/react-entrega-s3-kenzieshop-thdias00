import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { Products } from "../../assets";
import { listProducts } from "../../store/modules/Products/actions";
import LogoImg from "../../assets/Captura de tela de 2021-11-25 13-31-28.png";
import { AiOutlineShoppingCart } from "react-icons/ai";
import { Container } from "./style";
import { useHistory } from "react-router";
import { listCartThunk } from "../../store/modules/Cart/thunks";

const Home = () => {
    const dispatch = useDispatch();
    dispatch(listProducts(Products));
    const items = useSelector(state => state.products);
    const history = useHistory();

    function addToCart(item) {
        dispatch(listCartThunk(item));
    }

    return (
        <Container>
            <header>
                <img src={LogoImg} alt="Logo" />
                <AiOutlineShoppingCart size={50} onClick={() => history.push("/cart")} />
            </header>
            <main>
                <ul>
                    {items.map((item, index) =>
                        <li key={index}>
                            <img src={item.img} alt={item.name} />
                            <span>{item.name}</span>
                            <p>R$: {item.price}</p>
                            <button onClick={() => addToCart(item)}>Adicionar ao carrinho</button>
                        </li>)
                    }
                </ul>
            </main>
        </Container>
    )
}
export default Home;