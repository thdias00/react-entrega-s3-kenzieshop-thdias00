import LogoImg from "../../assets/Captura de tela de 2021-11-25 13-31-28.png";
import { IoArrowBackCircleOutline } from "react-icons/io5";
import { useHistory } from "react-router";
import { useSelector } from "react-redux";
import { RiDeleteBin5Line } from 'react-icons/ri'
import { Container } from "./style";
import { removeToCartThunk } from "../../store/modules/Cart/thunks";
import { useDispatch } from "react-redux";

const Cart = () => {
    const history = useHistory();
    const itensCart = useSelector(state => state.productCart);
    const dispatch = useDispatch();

    function removeItem(item) {
        dispatch(removeToCartThunk(item));
    }
    const total = itensCart.reduce((a, item) => {
        return a + parseFloat(item.price)
    }, 0)
    return (
        <Container>
            <header>
                <img src={LogoImg} alt="Logo" onClick={() => history.push("/")} />
                <IoArrowBackCircleOutline size={50} onClick={() => history.push("/")} />
            </header>
            <main>
                <div>
                    <h1>Carrinho de compras</h1>
                    <ul>
                        {itensCart.length > 0 ? itensCart.map((item, index) =>
                            <li key={index}>
                                <div>
                                    <img src={item.img} alt={item.name} />
                                    <span>{item.name}</span>
                                    <p>R$: {item.price}</p>
                                </div>
                                <RiDeleteBin5Line size={40} onClick={() => removeItem(item)} />
                            </li>
                        ) :
                            <div>
                                <h2>Seu Carrinho está vazio</h2>
                                <button onClick={() => history.push("/")}>Ir às compras</button>
                            </div>
                        }
                    </ul>
                </div>
                <section>
                    <h2>Total: R$ {total}</h2>
                </section>
            </main>
        </Container>
    )
}
export default Cart;